raspihats package
=================

Submodules
----------

raspihats.crc16 module
----------------------

.. automodule:: raspihats.crc16
    :members:
    :undoc-members:
    :show-inheritance:

raspihats.i2c_frame module
--------------------------

.. automodule:: raspihats.i2c_frame
    :members:
    :undoc-members:
    :show-inheritance:

raspihats.i2c_hat_modules module
--------------------------------

.. automodule:: raspihats.i2c_hat_modules
    :members:
    :undoc-members:
    :show-inheritance:

raspihats.i2c_hats module
-------------------------

.. automodule:: raspihats.i2c_hats
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: raspihats
    :members:
    :undoc-members:
    :show-inheritance:
