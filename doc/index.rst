.. Raspihats documentation master file, created by
   sphinx-quickstart on Wed Mar  2 11:45:49 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Raspihats's documentation!
=====================================

Inheritance diagram:

.. inheritance-diagram:: raspihats.i2c_hats.Rly10 raspihats.i2c_hats.Di16 raspihats.i2c_hats.Di6Rly6
   :parts: 1

Contents:

.. toctree::
   :maxdepth: 2
   
    modules <modules.rst>        



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

