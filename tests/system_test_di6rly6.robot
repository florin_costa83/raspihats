*** Settings ***
Documentation     Test Suite for Di6Rly6 I2C-HAT.
Library           Collections
Library           test_helper.py    # Library    raspihats.i2c_hats.Di6Rly6    ${0X60}    WITH NAME    I2CHat

*** Variables ***
${i2c_hat}        ${EMPTY}

*** Test Cases ***
Enable CWDT
    ${i2c_hat_temp} =    New Di6rly6
    Set Global Variable    ${i2c_hat}    ${i2c_hat_temp}
    Cwdt Set Period    ${4}

Board Name
    ${board_name} =    Get BoardName
    Should Be Equal As Strings    ${board_name}    Di6Rly6 I2C-HAT

Status
    ${status} =    Get StatusWord
    Reset
    Sleep    1s
    ${status} =    Get StatusWord
    Should Be Equal As Integers    ${status}    2
    ${status} =    Get StatusWord
    Should Be Equal As Integers    ${status}    0
    Reset
    Sleep    1s
    ${status} =    Get StatusWord
    Should Be Equal As Integers    ${status}    2
    ${status} =    Get StatusWord
    Should Be Equal As Integers    ${status}    0

Communication WatchDog Period
    Sleep    1s
    ${set_cwdt_period} =    Convert To Integer    7
    Cwdt Set Period    ${set_cwdt_period}
    ${get_cwdt_period} =    Cwdt Get Period
    Should Be Equal As Integers    ${get_cwdt_period}    ${set_cwdt_period}
    Reset
    Sleep    1s
    ${get_cwdt_period} =    Cwdt Get Period
    Should Be Equal As Integers    ${get_cwdt_period}    ${set_cwdt_period}
    ${set_cwdt_period} =    Convert To Integer    5
    Cwdt Set Period    ${set_cwdt_period}
    ${get_cwdt_period} =    Cwdt Get Period
    Should Be Equal As Integers    ${get_cwdt_period}    ${set_cwdt_period}

Communication WatchDog Period Timeout
    ${cwdt_period} =    Cwdt Get Period
    ${safety_value} =    Convert To Integer    7
    DO Set SafetyValue    ${safety_value}
    ${do_set_value} =    Convert To Integer    0
    DO Set All Channel States    ${do_set_value}
    # sleep 95% * CWDT Period, just to NOT load the SafeValue
    ${sleep_period} =    Evaluate    ${cwdt_period} * 0.95
    ${string_sleep_period} =    Convert To String    ${sleep_period}
    ${do_get_value} =    DO Get All Channel States
    Should Be Equal As Integers    ${do_set_value}    ${do_get_value}
    # sleep 105% * CWDT Period, just to load the SafeValue
    ${sleep_period} =    Evaluate    ${cwdt_period} * 1.05
    ${string_sleep_period} =    Convert To String    ${sleep_period}
    Sleep    ${string_sleep_period}
    ${do_get_value} =    DO Get All Channel States
    Should Be Equal As Integers    ${safety_value}    ${do_get_value}

DigitalOutputs PowerOnValue
    ${set_power_on_value} =    Convert To Integer    2
    DO Set PowerOnValue    ${set_power_on_value}
    ${get_power_on_value} =    DO Get PowerOnValue
    Should Be Equal As Integers    ${get_power_on_value}    ${set_power_on_value}
    Reset
    Sleep    1s
    ${get_power_on_value} =    DO Get PowerOnValue
    Should Be Equal As Integers    ${get_power_on_value}    ${set_power_on_value}

DigitalOutputs SafetyValue
    ${set_safety_value} =    Convert To Integer    2
    DO Set SafetyValue    ${set_safety_value}
    ${get_safety_value} =    DO Get SafetyValue
    Should Be Equal As Integers    ${get_safety_value}    ${set_safety_value}
    Reset
    Sleep    1s
    ${get_safety_value} =    DO Get SafetyValue
    Should Be Equal As Integers    ${get_safety_value}    ${set_safety_value}
    ${do_set_value} =    Convert To Integer    0
    DO Set All Channel States    ${do_set_value}
    # wait for Communication Watchdog Timeout, this should load the Safety Value
    Sleep    6s
    ${do_get_value} =    DO Get All Channel States
    Should Be Equal As Integers    ${set_safety_value}    ${do_get_value}

Disable CWDT
    Cwdt Set Period    ${0}
    ${safety_value} =    Convert To Integer    7
    DO Set SafetyValue    ${safety_value}
    ${do_set_value} =    Convert To Integer    0
    DO Set All Channel States    ${do_set_value}
    Sleep    10s
    # check that channel sates are preserved
    ${do_get_value} =    DO Get All Channel States
    Should Be Equal As Integers    ${do_get_value}    ${do_set_value}
    Sleep    10s
    # check that channel sates are preserved
    ${do_get_value} =    DO Get All Channel States
    Should Be Equal As Integers    ${do_get_value}    ${do_set_value}

DigitalOutputs SetGet All Channels
    ${do_set_value} =    Convert To Integer    0
    DO Set All Channel States    ${do_set_value}
    ${do_get_value} =    DO Get All Channel States
    Should Be Equal As Integers    ${do_set_value}    ${do_get_value}
    ${do_set_value} =    Convert To Integer    0x3F
    DO Set All Channel States    ${do_set_value}
    ${do_get_value} =    DO Get All Channel States
    Should Be Equal As Integers    ${do_set_value}    ${do_get_value}

DigitalOutputs SetGet Single Channel
    ${do_set_value} =    Convert To Integer    0
    DO Set All Channel States    ${do_set_value}
    : FOR    ${channel}    IN RANGE    0    6
    \    ${do_get_value} =    DO Get Channel State    ${channel}
    \    Should Be Equal As Integers    ${do_get_value}    ${0}
    : FOR    ${channel}    IN RANGE    0    6
    \    DO Set Channel State    ${channel}    ${1}
    \    ${do_get_value} =    DO Get Channel State    ${channel}
    \    Should Be Equal As Integers    ${do_get_value}    ${1}
    \    ${do_get_value} =    DO Get All Channel States
    \    ${do_get_value} =    Evaluate    ${do_get_value} >> ${channel}
    \    Should Be Equal As Integers    ${do_get_value}    ${1}

DigitalOutputsInputs SetGet All Channels
    ${do_value} =    Convert To Integer    0
    DO Set All Channel States    ${do_value}
    ${di_value} =    DI Get All Channel States
    Should Be Equal As Integers    ${do_value}    ${di_value}
    ${do_value} =    Convert To Integer    0x3F
    DO Set All Channel States    ${do_value}
    Sleep    0.01s
    ${di_value} =    DI Get All Channel States
    Should Be Equal As Integers    ${do_value}    ${di_value}

DigitalOutputsInputs SetGet Single Channel
    ${do_value} =    Convert To Integer    0
    DO Set All Channel States    ${do_value}
    ${do_value} =    Convert To Integer    1
    : FOR    ${channel}    IN RANGE    0    6
    \    DO Set Channel State    ${channel}    ${do_value}
    \    Sleep    0.01s
    \    ${di_value} =    DI Get Channel State    ${channel}
    \    Should Be Equal As Integers    ${do_value}    ${di_value}
    ${do_value} =    Convert To Integer    0
    : FOR    ${channel}    IN RANGE    0    6
    \    DO Set Channel State    ${channel}    ${do_value}
    \    Sleep    0.01s
    \    ${di_value} =    DI Get Channel State    ${channel}
    \    Should Be Equal As Integers    ${do_value}    ${di_value}

DigitalInputs Counter
    ${do_value} =    Convert To Integer    0
    DO Set All Channel States    ${do_value}
    DI Reset All Counters
    : FOR    ${channel}    IN RANGE    0    6
    # generate counter rising edge
    \    ${do_value} =    Convert To Integer    1
    \    DO Set Channel State    ${channel}    ${do_value}
    \    Sleep    0.01s
    # check rising edge counter
    \    ${counter_type} =    Convert To Integer    1
    \    ${counter} =    DI Get Counter    ${channel}    ${counter_type}
    \    Should Be Equal As Integers    ${counter}    1
    # check falling edge counter
    \    ${counter_type} =    Convert To Integer    0
    \    ${counter} =    DI Get Counter    ${channel}    ${counter_type}
    \    Should Be Equal As Integers    ${counter}    0
    # generate counter falling edge
    \    ${do_value} =    Convert To Integer    0
    \    DO Set Channel State    ${channel}    ${do_value}
    \    Sleep    0.01s
    # check rising edge counter
    \    ${counter_type} =    Convert To Integer    1
    \    ${counter} =    DI Get Counter    ${channel}    ${counter_type}
    \    Should Be Equal As Integers    ${counter}    1
    # check falling edge counter
    \    ${counter_type} =    Convert To Integer    0
    \    ${counter} =    DI Get Counter    ${channel}    ${counter_type}
    \    Should Be Equal As Integers    ${counter}    1
    # reset all counters using single counter reset
    : FOR    ${channel}    IN RANGE    0    6
    \    ${counter_type} =    Convert To Integer    1
    \    DI Reset Counter    ${channel}    ${counter_type}
    \    ${counter} =    DI Get Counter    ${channel}    ${counter_type}
    \    Should Be Equal As Integers    ${counter}    0
    \    ${counter_type} =    Convert To Integer    0
    \    ${counter} =    DI Get Counter    ${channel}    ${counter_type}
    \    Should Not Be Equal As Integers    ${counter}    0
    \    DI Reset Counter    ${channel}    ${counter_type}
    \    ${counter} =    DI Get Counter    ${channel}    ${counter_type}
    \    Should Be Equal As Integers    ${counter}    0
    ${do_value} =    Convert To Integer    0
    DO Set All Channel States    ${do_value}
    DI Reset All Counters
    ${cycles} =    Convert To Integer    300
    : FOR    ${channel}    IN RANGE    0    ${cycles}
    # generate counter rising edge
    \    ${do_value} =    Convert To Integer    0x3F
    \    DO Set All Channel States    ${do_value}
    \    Sleep    0.01s
    # generate counter falling edge
    \    ${do_value} =    Convert To Integer    0
    \    DO Set All Channel States    ${do_value}
    \    Sleep    0.01s
    # check rising edge counters
    ${counter_type} =    Convert To Integer    1
    : FOR    ${channel}    IN RANGE    0    6
    \    ${counter} =    DI Get Counter    ${channel}    ${counter_type}
    \    Should Be Equal As Integers    ${counter}    ${cycles}
    # check falling edge counters
    ${counter_type} =    Convert To Integer    0
    : FOR    ${channel}    IN RANGE    0    6
    \    ${counter} =    DI Get Counter    ${channel}    ${counter_type}
    \    Should Be Equal As Integers    ${counter}    ${cycles}

Set Default Values
    DO Set PowerOnValue     ${0}
    DO Set SafetyValue      ${0}
    # CWDT disabled
    Cwdt Set Period         ${0}


*** Keywords ***
Get BoardName
    ${board_name}    Call Method    ${i2c_hat}    get_board_name
    [Return]    ${board_name}

Reset
    Call Method    ${i2c_hat}    reset

Get StatusWord
    ${status_word} =    Call Method    ${i2c_hat}    get_status_word
    [Return]    ${status_word}

Cwdt Get Period
    ${status_word} =    Call Method    ${i2c_hat}    cwdt_get_period
    [Return]    ${status_word}

Cwdt Set Period
    [Arguments]    ${cwdt_period}
    Call Method    ${i2c_hat}    cwdt_set_period    ${cwdt_period}

DO Get PowerOnValue
    ${do_power_on_value} =    Call Method    ${i2c_hat}    do_get_power_on_value
    [Return]    ${do_power_on_value}

DO Set PowerOnValue
    [Arguments]    ${do_power_on_value}
    Call Method    ${i2c_hat}    do_set_power_on_value    ${do_power_on_value}

DO Get SafetyValue
    ${do_safety_value} =    Call Method    ${i2c_hat}    do_get_safety_value
    [Return]    ${do_safety_value}

DO Set SafetyValue
    [Arguments]    ${do_safety_value}
    Call Method    ${i2c_hat}    do_set_safety_value    ${do_safety_value}

DO Get All Channel States
    ${do_value} =    Call Method    ${i2c_hat}    do_get_all_states
    [Return]    ${do_value}

DO Set All Channel States
    [Arguments]    ${do_value}
    Call Method    ${i2c_hat}    do_set_all_states    ${do_value}

DO Get Channel State
    [Arguments]    ${channel}
    ${do_value} =    Call Method    ${i2c_hat}    do_get_state    ${channel}
    [Return]    ${do_value}

DO Set Channel State
    [Arguments]    ${channel}    ${do_value}
    Call Method    ${i2c_hat}    do_set_state    ${channel}    ${do_value}

DI Get All Channel States
    ${di_value} =    Call Method    ${i2c_hat}    di_get_all_states
    [Return]    ${di_value}

DI Get Channel State
    [Arguments]    ${channel}
    ${di_value} =    Call Method    ${i2c_hat}    di_get_state    ${channel}
    [Return]    ${di_value}

DI Get Counter
    [Arguments]    ${channel}    ${counter_type}
    ${di_counter} =    Call Method    ${i2c_hat}    di_get_counter    ${channel}    ${counter_type}
    [Return]    ${di_counter}

DI Reset Counter
    [Arguments]    ${channel}    ${counter_type}
    Call Method    ${i2c_hat}    di_reset_counter    ${channel}    ${counter_type}

DI Reset All Counters
    Call Method    ${i2c_hat}    di_reset_all_counters
